﻿using Proyecto.Repositories.Cupones;

namespace Proyecto.UnitOfWork
{
    public interface IUnitOfWork
    {
        IUsuarioRepository Usuario { get; }
        ICampanhaRepository Campanha { get; }
        IBannerRepository Banner { get; }
        IBannerKioskoRepository BannerKiosko { get; }
        IBannerClienteRepository BannerCliente { get; }
        IBannerProgramacionRepository BannerProgramacion { get; }
        IConfiguracionRepository Configuracion { get; }
    }
}
