﻿using Dapper;
using Proyecto.Models;
using Proyecto.Repositories.Cupones;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Proyecto.Repositories.Dapper.Cupones
{
    public class ConfiguracionRespository : Repository<Config>, IConfiguracionRepository
    {
        public ConfiguracionRespository(string connectionString) : base(connectionString)
        {
        }

        public IEnumerable<Config> List()
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var lstConfig = connection.Query<Config>("SELECT * FROM Perfil.Config");
                return lstConfig;
            }
        }
        
    }
}
