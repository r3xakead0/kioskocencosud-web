﻿using Dapper;
using Proyecto.Models;
using Proyecto.Repositories.Cupones;
using System.Data.SqlClient;

namespace Proyecto.Repositories.Dapper.Cupones
{
    public class UsuarioRepository : Repository<Usuario>, IUsuarioRepository
    {
        public UsuarioRepository(string connectionString) : base(connectionString)
        {
        }

        public Usuario ValidateUser(string username, string password)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var parameters = new DynamicParameters();
                parameters.Add("@username", username);
                parameters.Add("@password", password);

                var user = connection.QueryFirstOrDefault<Usuario>("dbo.ValidateUsuario", parameters, commandType: System.Data.CommandType.StoredProcedure);
                return user;
            }
        }
    }
}
