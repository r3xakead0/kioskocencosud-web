﻿using Dapper;
using Dapper.Contrib.Extensions;
using Proyecto.Models;
using Proyecto.Repositories.Cupones;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Proyecto.Repositories.Dapper.Cupones
{
    public class CampanhaRespository : Repository<Campanha>, ICampanhaRepository
    {
        public CampanhaRespository(string connectionString) : base(connectionString)
        {
        }

        public IEnumerable<Campanha> Combo()
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var lstCampanhas = connection.Query<Campanha>("SELECT * FROM Maestro.Campanha WHERE Activo = 1");
                return lstCampanhas;
            }
        }


        public IEnumerable<Campanha> List(Campanha entity, int start, int end)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var parameters = new DynamicParameters();
                parameters.Add("@nombre", entity.Nombre);
                parameters.Add("@start", start);
                parameters.Add("@end", end);

                var lstCampanhas = connection.Query<Campanha>("Kiosko.GetAllCampanhas", parameters, commandType: System.Data.CommandType.StoredProcedure);
                return lstCampanhas;
            }
        }

        public int CountList(Campanha entity)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var parameters = new DynamicParameters();

                parameters.Add("@nombre", entity.Nombre);

                return connection.ExecuteScalar<int>("Kiosko.GetCountAllCampanhas", parameters, commandType: System.Data.CommandType.StoredProcedure);//lProducts.Count();
            }
        }

        public Campanha GetCampanhaById(int Id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection.Get<Campanha>(Id);
            }
        }

        public int InsertCampanha(Campanha entity)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return (int)connection.Insert(entity);
            }
        }

        public bool UpdateCampanha(Campanha entity)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return (bool)connection.Update(entity);
            }
        }

        public bool DeleteCampanha(Campanha entity)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return (bool)connection.Delete(entity);
            }
        }

       
    }
}
