﻿using Proyecto.Repositories.Cupones;
using Proyecto.UnitOfWork;

namespace Proyecto.Repositories.Dapper.Cupones
{
    public class CuponesUnitOfWork : IUnitOfWork
    {
        public CuponesUnitOfWork(string connectionString)
        {
            Usuario = new UsuarioRepository(connectionString);
            Campanha = new CampanhaRespository(connectionString);
            Banner = new BannerRespository(connectionString);
            BannerKiosko = new BannerKioskoRespository(connectionString);
            BannerCliente = new BannerClienteRespository(connectionString);
            BannerProgramacion = new BannerProgramacionRespository(connectionString);
            Configuracion = new ConfiguracionRespository(connectionString);
        }

        public ICampanhaRepository Campanha { get; set; }
        public IBannerRepository Banner { get; set; }
        public IUsuarioRepository Usuario { get; set; }
        public IBannerKioskoRepository BannerKiosko { get; set; }
        public IBannerClienteRepository BannerCliente { get; set; }
        public IConfiguracionRepository Configuracion { get; set; }
        public IBannerProgramacionRepository BannerProgramacion { get; set; }
    }
}
