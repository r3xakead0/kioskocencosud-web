﻿using Dapper;
using Dapper.Contrib.Extensions;
using Proyecto.Models;
using Proyecto.Repositories.Cupones;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Proyecto.Repositories.Dapper.Cupones
{
    public class BannerKioskoRespository : Repository<BannerKiosko>, IBannerKioskoRepository
    {
        public BannerKioskoRespository(string connectionString) : base(connectionString)
        {
        }

        public IEnumerable<string> ComboTiendas(int idBanner)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var lstTiendas = connection.Query<string>($"SELECT DISTINCT Tienda FROM Banner.BannerKiosko WHERE IdBanner = {idBanner}");
                return lstTiendas;
            }
        }


        public string GetCompania(string tienda)
        {
            string compania = "";
            using (var connection = new SqlConnection(_connectionString))
            {
                var lstCompania = connection.Query<string>($"SELECT TOP 1 Compania FROM Perfil.Config WHERE Tienda = '{tienda}'");
                compania = lstCompania.AsList<string>()[0];
            }
            return compania;
        }

        public bool DeleteBannerCliente(BannerKiosko entity)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return (bool)connection.Delete(entity);
            }
        }

        public int InsertBannerCliente(BannerKiosko entity)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return (int)connection.Insert(entity);
            }
        }

        public IEnumerable<BannerKiosko> List(int idBanner)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var lstBannerKiosko = connection.Query<BannerKiosko>($"SELECT * FROM Banner.BannerKiosko WHERE IdBanner = {idBanner}");
                return lstBannerKiosko;
            }
        }

        public IEnumerable<BannerKiosko> List(int idBanner, string tienda)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var lstHostnames = connection.Query<BannerKiosko>($"SELECT * FROM Banner.BannerKiosko WHERE IdBanner = {idBanner} AND Tienda = '{tienda}'");
                return lstHostnames;
            }
        }

    }
}
