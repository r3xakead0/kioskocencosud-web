﻿using Dapper;
using Dapper.Contrib.Extensions;
using Proyecto.Models;
using Proyecto.Repositories.Cupones;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Proyecto.Repositories.Dapper.Cupones
{
    public class BannerClienteRespository : Repository<BannerCliente>, IBannerClienteRepository
    {
        public BannerClienteRespository(string connectionString) : base(connectionString)
        {
        }

        public bool DeleteBannerCliente(BannerCliente entity)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return (bool)connection.Delete(entity);
            }
        }


        public int InsertBannerCliente(BannerCliente entity)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return (int)connection.Insert(entity);
            }
        }

        public IEnumerable<BannerCliente> ListBannerCliente(BannerCliente entity, int start, int end)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                string sql = "";

                sql += $"SELECT T1.IdBannerCliente, T1.IdBanner, T1.TipoDocumento, T1.NroDocumento, T1.FechaRegistro, T1.Sincronizado FROM (" +
                    $"SELECT T0.IdBannerCliente, T0.IdBanner, T0.TipoDocumento, T0.NroDocumento, T0.FechaRegistro, T0.Sincronizado, " +
                    $"ROW_NUMBER() OVER(ORDER BY T0.IdBannerCliente) AS RowNum " +
                    $"FROM Banner.BannerCliente T0 " +
                    $"WHERE T0.IdBanner = {entity.IdBanner}";

                if (entity.TipoDocumento != null && entity.TipoDocumento.Length > 0)
                    sql += $" AND T0.TipoDocumento = '{entity.TipoDocumento}'";

                if (entity.NroDocumento != null && entity.NroDocumento.Length > 0)
                    sql += $" AND T0.NroDocumento LIKE '%{entity.NroDocumento}%'";

                sql += $") T1 WHERE T1.RowNum > {start} AND T1.RowNum <= ({start} + {end})";

                var lstBannerKiosko = connection.Query<BannerCliente>(sql);
                return lstBannerKiosko;
            }
        }

        public int CountBannerCliente(BannerCliente entity)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                string sql = $"SELECT COUNT(1) " +
                    $"FROM Banner.BannerCliente T0 " +
                    $"WHERE T0.IdBanner = {entity.IdBanner}";

                if (entity.TipoDocumento != null && entity.TipoDocumento.Length > 0)
                    sql += $" AND T0.TipoDocumento = '{entity.TipoDocumento}'";

                if (entity.NroDocumento != null && entity.NroDocumento.Length > 0)
                    sql += $" AND T0.NroDocumento LIKE '%{entity.NroDocumento}%'";

                var countBannerKiosko = connection.ExecuteScalar<int>(sql);

                return countBannerKiosko;
            }
        }

        public bool UpdateBannerCliente(BannerCliente entity)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return (bool)connection.Update(entity);
            }
        }
    }
}
