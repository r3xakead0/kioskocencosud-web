﻿using Dapper;
using Dapper.Contrib.Extensions;
using Proyecto.Models;
using Proyecto.Repositories.Cupones;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Proyecto.Repositories.Dapper.Cupones
{
    public class BannerProgramacionRespository : Repository<BannerKioskoProgramacion>, IBannerProgramacionRepository
    {
        public BannerProgramacionRespository(string connectionString) : base(connectionString)
        {
        }

        public IEnumerable<BannerKioskoProgramacion> ListBannerProgramacion(BannerKioskoProgramacion entity, int start, int end)
        {
            var lstBannerProgramacion = new List<BannerKioskoProgramacion>();
            return lstBannerProgramacion;
        }

        public int CountBannerProgramacion(BannerKioskoProgramacion entity)
        {
            var cntBannerProgramacion = 0;
            return cntBannerProgramacion;
        }

        public bool DeleteBannerProgramacion(BannerKioskoProgramacion entity)
        {
            throw new System.NotImplementedException();
        }

        public int InsertBannerProgramacion(BannerKioskoProgramacion entity)
        {
            throw new System.NotImplementedException();
        }

        public bool UpdateBannerProgramacion(BannerKioskoProgramacion entity)
        {
            throw new System.NotImplementedException();
        }
    }
}
