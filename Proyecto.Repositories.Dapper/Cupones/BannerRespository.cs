﻿using Dapper;
using Dapper.Contrib.Extensions;
using Proyecto.Models;
using Proyecto.Repositories.Cupones;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Proyecto.Repositories.Dapper.Cupones
{
    public class BannerRespository : Repository<Banner>, IBannerRepository
    {
        public BannerRespository(string connectionString) : base(connectionString)
        {
        }

        public IEnumerable<Banner> Combo(int idCampanha)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var lstBanners = connection.Query<Banner>($"SELECT * FROM Maestro.Banner WHERE Activo = 1 AND IdCampanha = {idCampanha}");
                return lstBanners;
            }
        }

        public IEnumerable<Banner> List(Banner entity, int start, int end)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var parameters = new DynamicParameters();
                parameters.Add("@nombre", entity.Nombre);
                parameters.Add("@start", start);
                parameters.Add("@end", end);

                var lstBanners = connection.Query<Banner>("Kiosko.GetAllBanners", parameters, commandType: System.Data.CommandType.StoredProcedure);
                return lstBanners;
            }
        }

        public int CountList(Banner entity)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var parameters = new DynamicParameters();

                parameters.Add("@nombre", entity.Nombre);

                return connection.ExecuteScalar<int>("Kiosko.GetCountAllBanners", parameters, commandType: System.Data.CommandType.StoredProcedure);//lProducts.Count();
            }
        }

        public Banner GetBannerById(int Id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection.Get<Banner>(Id);
            }
        }

        public int InsertBanner(Banner entity)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return (int)connection.Insert(entity);
            }
        }

        public bool UpdateBanner(Banner entity)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return (bool)connection.Update(entity);
            }
        }

        public bool DeleteBanner(Banner entity)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return (bool)connection.Delete(entity);
            }
        }
        
    }
}
