﻿using Proyecto.Models;
using Proyecto.Models.ViewModels;
using System.Collections.Generic;

namespace Proyecto.Repositories.Cupones
{
    public interface ICampanhaRepository : IRepository<Campanha>
    {

        IEnumerable<Campanha> Combo();

        IEnumerable<Campanha> List(Campanha entity, int start, int end);
        int CountList(Campanha entity);

        Campanha GetCampanhaById(int Id);
        int InsertCampanha(Campanha entity);
        bool UpdateCampanha(Campanha entity);
        bool DeleteCampanha(Campanha entity);
    }
}
