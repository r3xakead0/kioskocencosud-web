﻿using Proyecto.Models;
using Proyecto.Models.ViewModels;
using System.Collections.Generic;

namespace Proyecto.Repositories.Cupones
{
    public interface IConfiguracionRepository : IRepository<Config>
    {
        IEnumerable<Config> List();
    }
}
