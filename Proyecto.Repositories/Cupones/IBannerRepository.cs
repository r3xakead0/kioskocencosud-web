﻿using Proyecto.Models;
using System.Collections.Generic;

namespace Proyecto.Repositories.Cupones
{
    public interface IBannerRepository : IRepository<Banner>
    {

        IEnumerable<Banner> Combo(int idCampanha);

        IEnumerable<Banner> List(Banner entity, int start, int end);
        int CountList(Banner entity);

        Banner GetBannerById(int Id);
        int InsertBanner(Banner entity);
        bool UpdateBanner(Banner entity);
        bool DeleteBanner(Banner entity);
    }
}
