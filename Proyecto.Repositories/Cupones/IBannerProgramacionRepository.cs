﻿using Proyecto.Models;
using System.Collections.Generic;

namespace Proyecto.Repositories.Cupones
{
    public interface IBannerProgramacionRepository : IRepository<BannerKioskoProgramacion>
    {
        IEnumerable<BannerKioskoProgramacion> ListBannerProgramacion(BannerKioskoProgramacion entity, int start, int end);
        int CountBannerProgramacion(BannerKioskoProgramacion entity);

        int InsertBannerProgramacion(BannerKioskoProgramacion entity);
        bool UpdateBannerProgramacion(BannerKioskoProgramacion entity);
        bool DeleteBannerProgramacion(BannerKioskoProgramacion entity);
    }
}
