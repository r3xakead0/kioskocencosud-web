﻿using Proyecto.Models;
using System.Collections.Generic;

namespace Proyecto.Repositories.Cupones
{
    public interface IBannerKioskoRepository : IRepository<BannerKiosko>
    {
        IEnumerable<string> ComboTiendas(int idBanner);
        

        string GetCompania(string tienda);
        IEnumerable<BannerKiosko> List(int idBanner);
        IEnumerable<BannerKiosko> List(int idBanner, string tienda);
        int InsertBannerCliente(BannerKiosko entity);
        bool DeleteBannerCliente(BannerKiosko entity);
    }
}
