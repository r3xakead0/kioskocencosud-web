﻿using Proyecto.Models;
using System.Collections.Generic;

namespace Proyecto.Repositories.Cupones
{
    public interface IBannerClienteRepository : IRepository<BannerCliente>
    {
        IEnumerable<BannerCliente> ListBannerCliente(BannerCliente entity, int start, int end);
        int CountBannerCliente(BannerCliente entity);
        int InsertBannerCliente(BannerCliente entity);
        bool UpdateBannerCliente(BannerCliente entity);
        bool DeleteBannerCliente(BannerCliente entity);
    }
}
