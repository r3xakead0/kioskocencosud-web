﻿using Proyecto.Models;

namespace Proyecto.Repositories.Cupones
{
    public interface IUsuarioRepository : IRepository<Usuario>
    {
        Usuario ValidateUser(string usuario, string password);
    }
}
