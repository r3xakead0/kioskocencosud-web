﻿using System.Collections.Generic;

namespace Proyecto.Repositories
{
    public interface IRepository<T> where T : class
    {
        int Insert(T entity);
        bool Delete(T entity);
        bool Update(T entity);
        IEnumerable<T> GetList();
        T GetById(int Id);
    }
}
