﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Proyecto.Models;
using Proyecto.Models.ViewModels;
using Proyecto.UnitOfWork;
using Proyecto.MVC.ActionFilters;

namespace Proyecto.MVC.Controllers
{
    public class BannerProgramacionController : BaseController
    {

        public BannerProgramacionController(IUnitOfWork unit) : base(unit)
        {
        }

        public ActionResult Index()
        {
            return View();
        }

        [JsonNetFilter]
        public JsonResult List(BannerProgramacionVM bannerProgramacionVM, int start, int end)
        {

            var entity = new BannerKioskoProgramacion();
            entity.IdBannerKioskooProgramacion = bannerProgramacionVM.IdBannerKioskooProgramacion;
            entity.IdBannerKiosko = bannerProgramacionVM.IdBannerKiosko;
            entity.Fecha = bannerProgramacionVM.Fecha;
            entity.HoraInicio = bannerProgramacionVM.HoraInicio;
            entity.HoraFin = bannerProgramacionVM.HoraFin;
            entity.FechaRegistro = DateTime.Now;
            entity.Sincronizado = false;

            var lstBannerCliente = _unit.BannerProgramacion.ListBannerProgramacion(entity, start, end);
            var count = _unit.BannerProgramacion.CountBannerProgramacion(entity);

            var lstBannerProgramacionVM = new List<BannerProgramacionVM>();
            foreach (var oBanner in lstBannerCliente)
            {
                var entityVM = new BannerProgramacionVM();

                entityVM.IdBannerKioskooProgramacion = oBanner.IdBannerKioskooProgramacion;
                entityVM.IdBannerKiosko = oBanner.IdBannerKiosko;
                entityVM.Fecha = oBanner.Fecha;
                entityVM.HoraInicio = oBanner.HoraInicio;
                entityVM.HoraFin = oBanner.HoraFin;
                lstBannerProgramacionVM.Add(entityVM);
            }

            return Json(new { lstBannerProgramacionVM, count });
        }

        [HttpPost]
        public ActionResult Create(BannerProgramacionVM bannerProgramacionVM)
        {

            var entity = new BannerKioskoProgramacion();
            entity.IdBannerKioskooProgramacion = 0;
            entity.IdBannerKiosko = bannerProgramacionVM.IdBannerKiosko;
            entity.Fecha = bannerProgramacionVM.Fecha;
            entity.HoraInicio = bannerProgramacionVM.HoraInicio;
            entity.HoraFin = bannerProgramacionVM.HoraFin;
            entity.FechaRegistro = DateTime.Now;
            entity.Sincronizado = false;

            if (ModelState.IsValid)
            {
                _unit.BannerProgramacion.Insert(entity);
            }

            return Json(new { option = "create" });
        }

        [HttpPost]
        public ActionResult Edit(BannerProgramacionVM bannerProgramacionVM)
        {

            var entity = new BannerKioskoProgramacion();
            entity.IdBannerKioskooProgramacion = bannerProgramacionVM.IdBannerKioskooProgramacion;
            entity.IdBannerKiosko = bannerProgramacionVM.IdBannerKiosko;
            entity.Fecha = bannerProgramacionVM.Fecha;
            entity.HoraInicio = bannerProgramacionVM.HoraInicio;
            entity.HoraFin = bannerProgramacionVM.HoraFin;
            entity.FechaRegistro = DateTime.Now;
            entity.Sincronizado = false;

            if (ModelState.IsValid)
            {
                _unit.BannerProgramacion.Update(entity);
            }

            return Json(new { option = "edit" });
        }

        [HttpPost]
        public ActionResult Delete(BannerProgramacionVM bannerProgramacionVM)
        {
            var entity = new BannerKioskoProgramacion();
            entity.IdBannerKioskooProgramacion = bannerProgramacionVM.IdBannerKioskooProgramacion;
            entity.IdBannerKiosko = bannerProgramacionVM.IdBannerKiosko;
            entity.Fecha = bannerProgramacionVM.Fecha;
            entity.HoraInicio = bannerProgramacionVM.HoraInicio;
            entity.HoraFin = bannerProgramacionVM.HoraFin;
            entity.FechaRegistro = DateTime.Now;
            entity.Sincronizado = false;

            if (ModelState.IsValid)
            {
                _unit.BannerProgramacion.Delete(entity);
            }

            return Json(new { option = "delete" });
        }

    }
}