﻿using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using Proyecto.Models;
using Proyecto.Models.ViewModels;
using Proyecto.UnitOfWork;

namespace Proyecto.MVC.Controllers
{
    public class AccountController : BaseController
    {
        public AccountController(IUnitOfWork unit) : base(unit)
        {
        }
        
        [AllowAnonymous]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(LoginVM model)
        {
            //var user = _unit.Usuarios.ValidateUser(model.Username, model.Password);
            //if (user == null)
            //{
            //    ModelState.AddModelError(string.Empty, "Usuario y/o contraseña inválidos");
            //    return View(model);
            //}

            //TEMPORAL
            var user = new Usuario()
            {
                Username = "TEST",
                FirstName = "TEST",
                LastName = "TEST",
                Id = 0,
                Roles = "Admin"
            };

            var identity = new ClaimsIdentity(new[] {
                new Claim(ClaimTypes.Name, user.Username),
                new Claim(ClaimTypes.Role, user.Roles),
                new Claim(ClaimTypes.Name, $"{user.FirstName} {user.LastName}"),
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()) },
                "ApplicationCookie");

            var context = Request.GetOwinContext();
            var authManager = context.Authentication;

            authManager.SignIn(identity);

            return RedirectToLocal(model.ReturnUrl);
        }

        public ActionResult Logout()
        {
            var context = Request.GetOwinContext();
            var authManager = context.Authentication;

            authManager.SignOut("ApplicationCookie");
            return RedirectToAction("Login", "Account");
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }

            return RedirectToAction("Index", "Home");
        }
    }
}