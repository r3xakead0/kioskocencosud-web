﻿using Proyecto.MVC.ActionFilters;
using Proyecto.UnitOfWork;
using System.Web.Mvc;

namespace Proyecto.MVC.Controllers
{
    [CustomAuthorize]
    public class BaseController : Controller
    {
        protected readonly IUnitOfWork _unit;

        public BaseController(IUnitOfWork unit)
        {
            _unit = unit;
        }
    }
}