﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Proyecto.Models;
using Proyecto.Models.ViewModels;
using Proyecto.UnitOfWork;

namespace Proyecto.MVC.Controllers
{
    public class BannerKioskoController : BaseController
    {

        public BannerKioskoController(IUnitOfWork unit) : base(unit)
        {
        }

        public JsonResult ComboTiendas(int idBanner)
        {
            var lLiendas = _unit.BannerKiosko.ComboTiendas(idBanner);
            return Json(lLiendas);
        }

        public JsonResult ComboHostnames(int idBanner, string tienda)
        {
            var lbanners = _unit.BannerKiosko.List(idBanner, tienda);
            return Json(lbanners);
        }

        public JsonResult List(int idBanner)
        {
            var lBanners = _unit.BannerKiosko.List(idBanner);

            var lstBannersVM = new List<BannerKioskoVM>();
            foreach (var oBanner in lBanners)
            {
                var objBannerVM = new BannerKioskoVM();

                objBannerVM.Compania = _unit.BannerKiosko.GetCompania(oBanner.Tienda);
                objBannerVM.IdBannerKiosko = oBanner.IdBannerKiosko;
                objBannerVM.IdBanner = oBanner.IdBanner;
                objBannerVM.Tienda = oBanner.Tienda;
                objBannerVM.Hostname = oBanner.Hostname;
                lstBannersVM.Add(objBannerVM);
            }

            return Json(lstBannersVM);
        }

        [HttpPost]
        public ActionResult Create(BannerKioskoVM bannerKiosko)
        {

            var entity = new BannerKiosko();
            entity.IdBannerKiosko = 0;
            entity.IdBanner = bannerKiosko.IdBanner;
            entity.Tienda = bannerKiosko.Tienda;
            entity.Hostname = bannerKiosko.Hostname;
            entity.FechaRegistro = DateTime.Now;
            entity.Sincronizado = false;

            if (ModelState.IsValid)
            {
                _unit.BannerKiosko.Insert(entity);
            }

            return Json(new { option = "create" });
        }

        [HttpPost]
        public ActionResult Edit(BannerKioskoVM bannerKiosko)
        {

            var entity = new BannerKiosko();
            entity.IdBannerKiosko = bannerKiosko.IdBannerKiosko;
            entity.IdBanner = bannerKiosko.IdBanner;
            entity.Tienda = bannerKiosko.Tienda;
            entity.Hostname = bannerKiosko.Hostname;
            entity.FechaRegistro = DateTime.Now;
            entity.Sincronizado = false;

            if (ModelState.IsValid)
            {
                _unit.BannerKiosko.Update(entity);
            }

            return Json(new { option = "edit" });
        }

        [HttpPost]
        public ActionResult Delete(BannerKioskoVM bannerKiosko)
        {
            var entity = new BannerKiosko();
            entity.IdBannerKiosko = bannerKiosko.IdBannerKiosko;
            entity.IdBanner = bannerKiosko.IdBanner;
            entity.Tienda = bannerKiosko.Tienda;
            entity.Hostname = bannerKiosko.Hostname;
            entity.FechaRegistro = DateTime.Now;
            entity.Sincronizado = false;

            if (ModelState.IsValid)
            {
                _unit.BannerKiosko.Delete(entity);
            }

            return Json(new { option = "delete" });
        }

    }
}