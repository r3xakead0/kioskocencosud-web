﻿using System;
using System.Web.Mvc;
using Proyecto.Models;
using Proyecto.Models.ViewModels;
using Proyecto.UnitOfWork;

namespace Proyecto.MVC.Controllers
{
    public class CampanhaController : BaseController
    {

        public CampanhaController(IUnitOfWork unit) : base(unit)
        {
        }

        public ActionResult Index()
        {
            return View();
        }


        public JsonResult Combo()
        {
            var lCampanhas = _unit.Campanha.Combo();
            return Json(lCampanhas);
        }

        public JsonResult List(CampanhaVM campanha, int start, int end)
        {
            var entity = new Campanha();
            entity.IdCampanha = campanha.IdCampanha;
            entity.Nombre = campanha.Nombre;
            entity.Descripcion = campanha.Descripcion;
            entity.Activo = campanha.Activo;
            entity.FechaRegistro = DateTime.Now;
            entity.Sincronizado = false;

            var lCampanhas = _unit.Campanha.List(entity, start, end);
            var count = _unit.Campanha.CountList(entity);

            return Json(new { lCampanhas, count });
        }
        

        [HttpPost]
        public ActionResult Create(CampanhaVM campanha)
        {

            var entity = new Campanha();
            entity.IdCampanha = 0;
            entity.Nombre = campanha.Nombre;
            entity.Descripcion = campanha.Descripcion;
            entity.Activo = campanha.Activo;
            entity.FechaRegistro = DateTime.Now;
            entity.Sincronizado = false;

            if (ModelState.IsValid)
            {
                _unit.Campanha.Insert(entity);
            }

            return Json(new { option = "create" });
        }

        [HttpPost]
        public ActionResult Edit(CampanhaVM campanha)
        {

            var entity = new Campanha();
            entity.IdCampanha = campanha.IdCampanha;
            entity.Nombre = campanha.Nombre;
            entity.Descripcion = campanha.Descripcion;
            entity.Activo = campanha.Activo;
            entity.FechaRegistro = DateTime.Now;
            entity.Sincronizado = false;

            if (ModelState.IsValid)
            {
                _unit.Campanha.Update(entity);
            }

            return Json(new { option = "edit" });
        }

        [HttpPost]
        public ActionResult Delete(CampanhaVM campanha)
        {
            var entity = new Campanha();
            entity.IdCampanha = campanha.IdCampanha;
            entity.Nombre = campanha.Nombre;
            entity.Descripcion = campanha.Descripcion;
            entity.Activo = campanha.Activo;
            entity.FechaRegistro = DateTime.Now;
            entity.Sincronizado = false;

            if (ModelState.IsValid)
            {
                _unit.Campanha.Delete(entity);
            }

            return Json(new { option = "delete" });
        }

    }
}