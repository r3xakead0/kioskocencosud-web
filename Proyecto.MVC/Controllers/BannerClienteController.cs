﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Proyecto.Models;
using Proyecto.Models.ViewModels;
using Proyecto.UnitOfWork;

namespace Proyecto.MVC.Controllers
{
    public class BannerClienteController : BaseController
    {

        public BannerClienteController(IUnitOfWork unit) : base(unit)
        {
        }

        public JsonResult List(BannerClienteVM bannerCliente, int start, int end)
        {

            var entity = new BannerCliente();
            entity.IdBannerCliente = 0;
            entity.IdBanner = bannerCliente.IdBanner;
            entity.TipoDocumento = bannerCliente.TipoDocumento;
            entity.NroDocumento = bannerCliente.NroDocumento;
            entity.FechaRegistro = DateTime.Now;
            entity.Sincronizado = false;

            var lstBannerCliente = _unit.BannerCliente.ListBannerCliente(entity, start, end);
            var count = _unit.BannerCliente.CountBannerCliente(entity);

            var lstBannerClienteVM = new List<BannerClienteVM>();
            foreach (var oBanner in lstBannerCliente)
            {
                var objBannerVM = new BannerClienteVM();

                objBannerVM.IdBannerCliente = oBanner.IdBannerCliente;
                objBannerVM.IdBanner = oBanner.IdBanner;
                objBannerVM.TipoDocumento = oBanner.TipoDocumento;
                objBannerVM.NroDocumento = oBanner.NroDocumento;
                lstBannerClienteVM.Add(objBannerVM);
            }

            return Json(new { lstBannerClienteVM, count });
        }

        [HttpPost]
        public ActionResult Create(BannerClienteVM bannerCliente)
        {

            var entity = new BannerCliente();
            entity.IdBannerCliente = 0;
            entity.IdBanner = bannerCliente.IdBanner;
            entity.TipoDocumento = bannerCliente.TipoDocumento;
            entity.NroDocumento = bannerCliente.NroDocumento;
            entity.FechaRegistro = DateTime.Now;
            entity.Sincronizado = false;

            if (ModelState.IsValid)
            {
                _unit.BannerCliente.Insert(entity);
            }

            return Json(new { option = "create" });
        }

        [HttpPost]
        public ActionResult Edit(BannerClienteVM bannerCliente)
        {

            var entity = new BannerCliente();
            entity.IdBannerCliente = bannerCliente.IdBannerCliente;
            entity.IdBanner = bannerCliente.IdBanner;
            entity.TipoDocumento = bannerCliente.TipoDocumento;
            entity.NroDocumento = bannerCliente.NroDocumento;
            entity.FechaRegistro = DateTime.Now;
            entity.Sincronizado = false;

            if (ModelState.IsValid)
            {
                _unit.BannerCliente.Update(entity);
            }

            return Json(new { option = "edit" });
        }

        [HttpPost]
        public ActionResult Delete(BannerClienteVM bannerCliente)
        {
            var entity = new BannerCliente();
            entity.IdBannerCliente = bannerCliente.IdBannerCliente;
            entity.IdBanner = bannerCliente.IdBanner;
            entity.TipoDocumento = bannerCliente.TipoDocumento;
            entity.NroDocumento = bannerCliente.NroDocumento;
            entity.FechaRegistro = DateTime.Now;
            entity.Sincronizado = false;

            if (ModelState.IsValid)
            {
                _unit.BannerCliente.Delete(entity);
            }

            return Json(new { option = "delete" });
        }

    }
}