﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Proyecto.UnitOfWork;

namespace Proyecto.MVC.Controllers
{
    public class ConfiguracionController : BaseController
    {

        public ConfiguracionController(IUnitOfWork unit) : base(unit)
        {
        }

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult Tiendas(string compania)
        {
            var lstTiendas = new List<string>();

            var lstConfig = _unit.Configuracion.List().OrderBy(x => x.Tienda);
            foreach (var objConfig in lstConfig)
            {
                if (objConfig.Compania.Equals(compania))
                {
                    lstTiendas.Add(objConfig.Tienda);
                }
            }

            lstTiendas = lstTiendas.Distinct().ToList();

            return Json(lstTiendas);
        }

        public JsonResult Hostnames(string tienda)
        {
            var lstHostnames = new List<string>();

            var lstConfig = _unit.Configuracion.List().OrderBy(x => x.Hostname);
            foreach (var objConfig in lstConfig)
            {
                if (objConfig.Tienda.Equals(tienda))
                {
                    lstHostnames.Add(objConfig.Hostname);
                }
            }

            lstHostnames = lstHostnames.Distinct().ToList();

            return Json(lstHostnames);
        }
    }
}