﻿using Proyecto.UnitOfWork;
using System.Web.Mvc;

namespace Proyecto.MVC.Controllers
{
    public class HomeController : Controller
    {
        protected readonly IUnitOfWork _unit;

        public HomeController(IUnitOfWork unit)
        {
            _unit = unit;
        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Unauthorized()
        {
            Response.StatusCode = 403;
            return View();
        }

    }
}