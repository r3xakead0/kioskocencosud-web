﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Web.Mvc;
using FluentFTP;
using Proyecto.Models;
using Proyecto.Models.ViewModels;
using Proyecto.MVC.ActionFilters;
using Proyecto.UnitOfWork;

namespace Proyecto.MVC.Controllers
{
    public class BannerController : BaseController
    {

        public BannerController(IUnitOfWork unit) : base(unit)
        {
        }

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult Combo(int idCampana)
        {
            var lBanners = _unit.Banner.Combo(idCampana);
            return Json(lBanners);
        }

        [JsonNetFilter]
        public JsonResult List(BannerVM banner, int start, int end)
        {
            var entity = new Banner();
            entity.IdBanner = banner.IdBanner;
            entity.IdCampanha = banner.IdCampanha;
            entity.Nombre = banner.Nombre;
            entity.Imagen = banner.Imagen;
            entity.Prioridad = banner.Prioridad;
            entity.Tiempo = banner.Tiempo;
            entity.InicioVigencia = banner .InicioVigencia;
            entity.FinalVigencia = banner.FinalVigencia;
            entity.Tipo = banner.Tipo;
            entity.VistasLimite = banner.VistasLimite;
            entity.VistasActual = banner.VistasActual;
            entity.Activo = banner.Activo;
            entity.CodigosMateriales = banner.CodigosMateriales;
            entity.FechaRegistro = DateTime.Now;
            entity.Sincronizado = false;

            var lBanners = _unit.Banner.List(entity, start, end);
            var count = _unit.Banner.CountList(entity);

            return Json(new { lBanners, count });
        }

        [HttpPost]
        public ActionResult Create(BannerVM banner)
        {

            var entity = new Banner();
            entity.IdBanner = 0;
            entity.IdCampanha = banner.IdCampanha;
            entity.Nombre = banner.Nombre;
            entity.Imagen = banner.Imagen;
            entity.Prioridad = banner.Prioridad;
            entity.Tiempo = banner.Tiempo;
            entity.InicioVigencia = banner.InicioVigencia;
            entity.FinalVigencia = banner.FinalVigencia;
            entity.Tipo = banner.Tipo;
            entity.VistasLimite = banner.VistasLimite;
            entity.VistasActual = banner.VistasActual;
            entity.Activo = banner.Activo;
            entity.CodigosMateriales = banner.CodigosMateriales;
            entity.FechaRegistro = DateTime.Now;
            entity.Sincronizado = false;

            if (ModelState.IsValid)
            {
                _unit.Banner.Insert(entity);

                string fileName = Path.GetFileName(entity.Imagen);
                string localFullPath = Server.MapPath($"~/images/banners/{fileName}");
                string ftpFullPath = Path.Combine("Imagenes", "Banners", fileName);

                using (var ftp = new FtpClient())
                {
                    ftp.Encoding = Encoding.GetEncoding(1252);
                    ftp.Host = "g200603sv06e";
                    ftp.Credentials = new NetworkCredential("eb9798", "R3xak3ad0");
                    ftp.RetryAttempts = 3;
                    ftp.UploadFile(localFullPath, ftpFullPath, FtpExists.Overwrite);
                }

            }

            return Json(new { option = "create" });
        }

        [HttpPost]
        public ActionResult Edit(BannerVM campanha)
        {

            var entity = new Banner();
            entity.IdBanner = campanha.IdBanner;
            entity.IdCampanha = campanha.IdCampanha;
            entity.Nombre = campanha.Nombre;
            entity.Imagen = campanha.Imagen;
            entity.Prioridad = campanha.Prioridad;
            entity.Tiempo = campanha.Tiempo;
            entity.InicioVigencia = campanha.InicioVigencia;
            entity.FinalVigencia = campanha.FinalVigencia;
            entity.Tipo = campanha.Tipo;
            entity.VistasLimite = campanha.VistasLimite;
            entity.VistasActual = campanha.VistasActual;
            entity.Activo = campanha.Activo;
            entity.CodigosMateriales = campanha.CodigosMateriales;
            entity.FechaRegistro = DateTime.Now;
            entity.Sincronizado = false;

            if (ModelState.IsValid)
            {
                _unit.Banner.Update(entity);

                string fileName = Path.GetFileName(entity.Imagen);
                string localFullPath = Server.MapPath($"~/images/banners/{fileName}");
                string ftpFullPath = Path.Combine("Imagenes", "Banners", fileName);

                using (var ftp = new FtpClient())
                {
                    ftp.Encoding = Encoding.GetEncoding(1252);
                    ftp.Host = "g200603sv06e";
                    ftp.Credentials = new NetworkCredential("eb9798", "R3xak3ad0");
                    ftp.RetryAttempts = 3;
                    ftp.UploadFile(localFullPath, ftpFullPath, FtpExists.Overwrite);
                }
            }

            return Json(new { option = "edit" });
        }

        [HttpPost]
        public ActionResult Delete(BannerVM campanha)
        {
            var entity = new Banner();
            entity.IdBanner = campanha.IdBanner;
            entity.IdCampanha = campanha.IdCampanha;
            entity.Nombre = campanha.Nombre;
            entity.Imagen = campanha.Imagen;
            entity.Prioridad = campanha.Prioridad;
            entity.Tiempo = campanha.Tiempo;
            entity.InicioVigencia = campanha.InicioVigencia;
            entity.FinalVigencia = campanha.FinalVigencia;
            entity.Tipo = campanha.Tipo;
            entity.VistasLimite = campanha.VistasLimite;
            entity.VistasActual = campanha.VistasActual;
            entity.Activo = campanha.Activo;
            entity.CodigosMateriales = campanha.CodigosMateriales;
            entity.FechaRegistro = DateTime.Now;
            entity.Sincronizado = false;

            if (ModelState.IsValid)
            {
                bool rpta = _unit.Banner.Delete(entity);
                if (rpta)
                {
                    string fileName = Path.GetFileName(entity.Imagen);

                    string localFullPath = Server.MapPath($"~/images/banners/{fileName}");
                    if (System.IO.File.Exists(localFullPath))
                        System.IO.File.Delete(localFullPath);

                    string ftpFullPath = Path.Combine("Imagenes", "Banners", fileName);
                    using (var ftp = new FtpClient())
                    {
                        ftp.Encoding = Encoding.GetEncoding(1252);
                        ftp.Host = "g200603sv06e";
                        ftp.Credentials = new NetworkCredential("eb9798", "R3xak3ad0");
                        ftp.RetryAttempts = 3;

                        if (ftp.FileExists(ftpFullPath))
                            ftp.DeleteFile(ftpFullPath);
                    }
                }
            }

            return Json(new { option = "delete" });
        }

        public ActionResult Upload()
        {
            if (Request.Files.Count != 0)
            {
                string fileName = "";

                if (Request.Browser.Browser.Contains("InternetExplorer"))
                    fileName = Path.GetFileName(Request.Files[0].FileName);
                else
                    fileName = Request.Files[0].FileName;

                string filePath = $"~/images/banners/{fileName}";
                Request.Files[0].SaveAs(Server.MapPath(filePath));
            }
            return View("Index");
        }
    }
}