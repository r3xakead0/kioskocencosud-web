﻿var programacionPaged = new DevExpress.data.CustomStore({
    load: function (loadOptions) {
        var def = $.Deferred();
        var name = '';
        if (loadOptions.filter) {
            var value = loadOptions.filter;
            switch (value[0]) {
                case 'Nombre':
                    console.log(value);
                    name = value[2];
                    break;
            }
        }
        var oCampanha = {
            Nombre: name
        };
        var start = loadOptions.skip;
        var end = loadOptions.take;
        var param = {
            entity: oCampanha,
            start: start,
            end: end
        };

        $.post('/Campanha/List', param, function (data) {
            def.resolve(data.lCampanhas, { totalCount: data.count });
        }, 'json');
        return def.promise();
    },
    insert: function (values) {
        return $.ajax({
            url: '/Campanha/Create',
            method: "POST",
            data: values
        })
    },
    update: function (key, values) {
        var object = $.extend({}, key, values);
        return $.ajax({
            url: '/Campanha/Edit',
            method: "POST",
            data: object
        })
    },
    remove: function (key) {
        return $.ajax({
            url: '/Campanha/Delete',
            method: "POST",
            data: key
        })
    }
});

$('#dgCampanhas').dxDataGrid({
    noDataText: "No existen datos",
    dataSource: {
        store: programacionPaged
    },
    editing: {
        allowAdding: true,
        allowDeleting: true,
        allowUpdating: true,
        mode: "form",
        texts: {
            addRow: "Agregar",
            cancelRowChanges: "Cancelar",
            deleteRow: "Eliminar",
            editRow: "Editar",
            saveRowChanges: "Guardar",
            confirmDeleteMessage: '¿Desea eliminar el registro?'
        }
    },
    remoteOperations: {
        paging: true,
        filtering: true
    },
    rowAlternationEnabled: true,
    loadPanel: {
        enabled: "auto",
        height: 100,
        showIndicator: true,
        showPane: true,
        text: "Cargarndo...",
        width: 200
    },
    filterRow: {
        visible: true,
        applyFilter: "auto",
        showOperationChooser: false
    },
    pager: {
        allowedPageSizes: "auto",
        infoText: "Pagina {0} de {1} ({2} registros)",
        showInfo: true,
        showNavigationButtons: true,
        showPageSizeSelector: false,
        visible: "auto"
    },
    paging: {
        enabled: true,
        pageIndex: 0,
        pageSize: 10
    },
    columns: [{
        dataField: 'Nombre',
        caption: 'Nombre',
        validationRules: [{ type: "required" }],
        width: 300
    },
    {
        dataField: 'Descripcion',
        caption: 'Descripcion',
        allowFiltering: false
    },
    {
        dataField: 'Activo',
        caption: 'Activo',
        width: 70,
        hidingPriority: 0,
        allowFiltering: false
    }
    ]
});
