﻿var campanhasCombo = {
    store: new DevExpress.data.CustomStore({
        key: "IdCampanha",
        loadMode: "raw",
        load: function () {
            return $.ajax({
                url: '/Campanha/Combo',
                method: 'POST',
                contentType: 'application/json',
                type: 'json'
            });
        }
    }),
    sort: "Nombre"
}

var programacionPaged = new DevExpress.data.CustomStore({
    load: function (loadOptions) {
        var oProgramacion = {
            IdBannerKiosko: 1
        };
        var start = loadOptions.skip;
        var end = loadOptions.take;
        var param = {
            bannerProgramacionVM: oProgramacion,
            start: start,
            end: end
        };

        var def = $.Deferred();
        $.post('/BannerProgramacion/List', param, function (data) {
            def.resolve(data.lCampanhas, { totalCount: data.count });
        }, 'json');
        return def.promise();
    },
    insert: function (values) {
        return $.ajax({
            url: '/BannerProgramacion/Create',
            method: "POST",
            data: values
        })
    },
    update: function (key, values) {
        var object = $.extend({}, key, values);
        return $.ajax({
            url: '/BannerProgramacion/Edit',
            method: "POST",
            data: object
        })
    },
    remove: function (key) {
        return $.ajax({
            url: '/BannerProgramacion/Delete',
            method: "POST",
            data: key
        })
    }
});

$("#frmFiltro").dxForm({
    formData: {
        Campanha: [],
        Banner: [],
        TiendaKiosko: '',
    },
    items: [
        {
            dataField: 'Campanha',
            caption: 'Campaña',
            editorType: "dxSelectBox",
            editorOptions: {
                dataSource: campanhasCombo,
                displayExpr: 'Nombre',
                valueExpr: 'IdCampanha',
                placeholder: 'Seleccione una Campaña',
                onValueChanged: function (data) {
                    $.post('/Banner/Combo', { idCampana: data.value }, function (data) {
                        $('#frmFiltro').dxForm('instance').getEditor('Banner').option('dataSource', data);
                    });
                }
            },
            validationRules: [{
                type: "required",
                message: "Campaña requerida"
            }],
        },{
            dataField: 'Banner',
            caption: 'Banners',
            editorType: "dxSelectBox",
            editorOptions: {
                dataSource: [],
                displayExpr: 'Nombre',
                valueExpr: 'IdBanner',
                placeholder: 'Seleccione un Banner',
                onValueChanged: function (data) {
                    $.post('/BannerKiosko/ComboTiendas', { idBanner: data.value }, function (data) {
                        $('#frmFiltro').dxForm('instance').getEditor('Tienda').option('dataSource', data);
                    });
                }
            },
            validationRules: [{
                type: "required",
                message: "Banner requerido"
            }],
        }, {
            dataField: 'Tienda',
            caption: 'Tienda',
            editorType: "dxSelectBox",
            editorOptions: {
                dataSource: [],
                placeholder: 'Seleccione una Tienda',
                onValueChanged: function (data) {
                    let bannerId = $('#frmFiltro').dxForm('instance').getEditor('Banner').option('value');
                    let tiendaId = data.value;
                    $.post('/BannerKiosko/ComboHostnames', { idBanner: bannerId, tienda: tiendaId }, function (data) {
                        $('#frmFiltro').dxForm('instance').getEditor('Kiosko').option('dataSource', data);
                    });
                }
            },
            validationRules: [{
                type: "required",
                message: "Tienda requerida"
            }],
        }, {
            dataField: 'Kiosko',
            caption: 'Kiosko',
            editorType: "dxSelectBox",
            editorOptions: {
                dataSource: [],
                displayExpr: 'Hostname',
                valueExpr: 'IdBannerKiosko',
            },
            validationRules: [{
                type: "required",
                message: "Kiosko requerido"
            }],
        }],
    readOnly: false,
    showColonAfterLabel: true,
    labelLocation: "top",
    minColWidth: 300,
    colCount: 2
});

$('#dgProgramacion').dxDataGrid({
    noDataText: "No existen datos",
    dataSource: {
        store: programacionPaged
    },
    editing: {
        allowAdding: true,
        allowDeleting: true,
        allowUpdating: true,
        mode: "form",
        texts: {
            addRow: "Agregar",
            cancelRowChanges: "Cancelar",
            deleteRow: "Eliminar",
            editRow: "Editar",
            saveRowChanges: "Guardar",
            confirmDeleteMessage: '¿Desea eliminar el registro?'
        }
    },
    remoteOperations: {
        paging: true,
        filtering: true
    },
    rowAlternationEnabled: true,
    loadPanel: {
        enabled: "auto",
        height: 100,
        showIndicator: true,
        showPane: true,
        text: "Cargarndo...",
        width: 200
    },
    filterRow: {
        visible: false
    },
    pager: {
        allowedPageSizes: "auto",
        infoText: "Pagina {0} de {1} ({2} registros)",
        showInfo: true,
        showNavigationButtons: true,
        showPageSizeSelector: false,
        visible: "auto"
    },
    paging: {
        enabled: true,
        pageIndex: 0,
        pageSize: 10
    },
    columns: [{
        dataField: 'Fecha',
        caption: 'Fecha',
        dataType: 'Date',
        format: 'dd/MM/yyyy',
        validationRules: [{ type: "required" }],
        allowFiltering: false
    },
    {
        dataField: 'HoraInicio',
        caption: 'Hora de Inicio',
        width: 150,
        dataType: 'DateTime',
        format: 'HH:mm:ss',
        validationRules: [{ type: "required" }],
        allowFiltering: false
    },
    {
        dataField: 'HoraFin',
        caption: 'Hora de Fin',
        width: 150,
        dataType: 'DateTime',
        format: 'HH:mm:ss',
        validationRules: [{ type: "required" }],
        allowFiltering: false
    }
    ]
});
