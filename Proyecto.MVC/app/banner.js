﻿var campanhasCombo = {
    store: new DevExpress.data.CustomStore({
        key: "IdCampanha",
        loadMode: "raw",
        load: function () {
            return $.ajax({
                url: '/Campanha/Combo',
                method: 'POST',
                contentType: 'application/json',
                type: 'json'
            });
        }
    }),
    sort: "Nombre"
}

var bannersPaged = new DevExpress.data.CustomStore({
    load: function (loadOptions) {
        var def = $.Deferred();
        var name = '';
        if (loadOptions.filter) {
            var value = loadOptions.filter;
            switch (value[0]) {
                case 'Nombre':
                    name = value[2];
                    break;
            }
        }
        var oBanner = {
            Nombre: name
        };
        var start = loadOptions.skip;
        var end = loadOptions.take;
        var param = {
            entity: oBanner,
            start: start,
            end: end
        };

        $.post('/Banner/List', param, function (data) {
            def.resolve(data.lBanners, { totalCount: data.count });
        }, 'json');
        return def.promise();
    },
    insert: function (values) {
        return $.ajax({
            url: '/Banner/Create',
            method: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(values)
        })
    },
    update: function (key, values) {
        var object = $.extend({}, key, values);
        return $.ajax({
            url: '/Banner/Edit',
            method: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(object)
        })
    },
    remove: function (key) {
        return $.ajax({
            url: '/Banner/Delete',
            method: 'POST',
            data: key
        })
    }
});

$('#dgBanners').dxDataGrid({
    elementAttr: {
        id: "gridBanner"
    },
    noDataText: 'No existen datos',
    dataSource: {
        store: bannersPaged
    },
    editing: {
        allowAdding: true,
        allowDeleting: true,
        allowUpdating: true,
        mode: 'form',
        texts: getTextsGrid()
    },
    remoteOperations: {
        paging: true,
        filtering: true
    },
    rowAlternationEnabled: true,
    onRowExpanding: function (e) {
        e.component.collapseAll(-1);
    },
    loadPanel: {
        enabled: 'auto',
        height: 100,
        showIndicator: true,
        showPane: true,
        text: 'Cargarndo...',
        width: 200
    },
    pager: {
        allowedPageSizes: 'auto',
        infoText: 'Pagina {0} de {1} ({2} registros)',
        showInfo: true,
        showNavigationButtons: true,
        showPageSizeSelector: false,
        visible: 'auto'
    },
    paging: {
        enabled: true,
        pageIndex: 0,
        pageSize: 10
    },
    columns: [
        {
            dataField: 'IdCampanha',
            caption: 'Campaña',
            width: 200,
            allowFiltering: false,
            validationRules: [{ type: "required" }],
            editorOptions: {
                searchEnabled: false
            },
            lookup: {
                dataSource: campanhasCombo,
                displayExpr: 'Nombre',
                valueExpr: 'IdCampanha'
            }
        },
        {
            dataField: 'Nombre',
            caption: 'Nombre',
            width: 200,
            validationRules: [{ type: "required" }],
        },
        {
            dataField: 'Imagen',
            caption: 'Archivo de Imagen',
            width: 200,
            validationRules: [{ type: "required" }],
            allowFiltering: false,
            visible: false,
            editCellTemplate: function (cellElement, cellInfo) {
                console.log(cellInfo.value);
                $("<div/>")
                    .dxFileUploader({
                        elementAttr: {
                            id: "uploadBanner"
                        },
                        selectButtonText: "Seleccione",
                        labelText: cellInfo.value,
                        multiple: false,
                        accept: 'image/*',
                        uploadMode: "instantly",
                        uploadUrl: "Banner/Upload",
                        onValueChanged: function (e) {
                            cellInfo.setValue('Banners\\' + e.value[0].name);
                        }
                    })
                    .appendTo(cellElement);
            },
        },
        {
            dataField: 'Prioridad',
            caption: 'Prioridad',
            width: 80,
            validationRules: [{ type: "required" }],
            allowFiltering: false,
            editorOptions: {
                searchEnabled: false
            },
            lookup: {
                dataSource: [{ 'Id': 1, 'Nombre': 'Alta' }, { 'Id': 2, 'Nombre': 'Media' }, { 'Id': 3, 'Nombre': 'Baja' }],
                displayExpr: 'Nombre',
                valueExpr: 'Id'
            }
        },
        {
            dataField: 'Tiempo',
            caption: 'Segundos',
            width: 80,
            dataType: 'number',
            validationRules: [{ type: "required" }],
            allowFiltering: false,
        },
        {
            dataField: 'InicioVigencia',
            caption: 'Inicio de Vigencia',
            width: 150,
            dataType: 'date',
            format: 'dd/MM/yyyy',
            validationRules: [{ type: "required" }],
            allowFiltering: false
        },
        {
            dataField: 'FinalVigencia',
            caption: 'Final de Vigencia',
            width: 150,
            dataType: 'date',
            format: 'dd/MM/yyyy',
            validationRules: [{ type: "required" }],
            allowFiltering: false
        },
        {
            dataField: 'Tipo',
            caption: 'Tipo',
            width: 70,
            validationRules: [{ type: "required" }],
            allowFiltering: false,
            editorOptions: {
                searchEnabled: false
            },
            lookup: {
                dataSource: [{ 'Id': 'I', 'Nombre': 'Interno' }, { 'Id': 'E', 'Nombre': 'Externo' }],
                displayExpr: 'Nombre',
                valueExpr: 'Id'
            }
        },
        {
            dataField: 'CodigosMateriales',
            caption: 'Codigos de Materiales',
            validationRules: [{ type: "required" }],
            allowFiltering: false,
            visible: false
        },
        {
            dataField: 'Activo',
            caption: 'Activo',
            width: 70,
            dataType: 'boolean',
            allowFiltering: false
        }
    ],
    masterDetail: {
        enabled: true,
        template: function (container, options) {

            let idBanner = options.data.IdBanner;

            $("<div>")
                .dxTabPanel({
                    dataSource: [{
                        title: 'Kioskos',
                        template: function () {
                            return getTemplateGridKioskos(idBanner);
                        }
                    }, {
                        title: 'Clientes',
                        template: function () {
                            return getTemplateGridClientes(idBanner);
                        }
                    }],
                    selectedIndex: 0,
                })
                .appendTo(container);


        }
    }
});

function getTemplateGridKioskos(idBanner) {
    return $("<div id = 'gridBannerKiosko'>")
        .dxDataGrid({
            noDataText: 'No existen datos',
            dataSource: {
                load: function () {
                    var def = $.Deferred();
                    var param = {
                        idBanner: idBanner
                    };
                    $.post('/BannerKiosko/List', param, function (data) {
                        def.resolve(data);
                    }, 'json');
                    return def.promise();
                },
                insert: function (values) {
                    values.IdBanner = idBanner;
                    return $.ajax({
                        url: '/BannerKiosko/Create',
                        method: 'POST',
                        contentType: 'application/json',
                        data: JSON.stringify(values)
                    })
                },
                remove: function (key) {
                    return $.ajax({
                        url: '/BannerKiosko/Delete',
                        method: 'POST',
                        data: key
                    })
                },
            },
            showBorders: true,
            rowAlternationEnabled: true,
            editing: {
                allowAdding: true,
                allowDeleting: true,
                mode: 'form',
                texts: getTextsGrid()
            },
            remoteOperations: false,
            searchPanel: {
                visible: true,
                highlightCaseSensitive: true
            },
            loadPanel: {
                enabled: 'auto',
                height: 100,
                showIndicator: true,
                showPane: true,
                text: 'Cargarndo...',
                width: 200
            },
            pager: {
                allowedPageSizes: 'auto',
                infoText: 'Pagina {0} de {1} ({2} registros)',
                showInfo: true,
                showNavigationButtons: true,
                showPageSizeSelector: false,
                visible: 'auto'
            },
            paging: {
                enabled: true,
                pageIndex: 0,
                pageSize: 5
            },
            groupPanel: { visible: true },
            grouping: {
                autoExpandAll: false
            },
            columns: [
                {
                    dataField: "Compania",
                    caption: 'Compañia',
                    allowFiltering: false,
                    editorOptions: {
                        searchEnabled: false,
                        placeholder: 'Seleccione una Compañia',
                    },
                    lookup: {
                        dataSource: ['WONG', 'METRO']
                    },
                    setCellValue: function (rowData, value) {
                        var params = { compania: value };
                        $.post('/Configuracion/Tiendas', params, function (data) {
                            var grid = $("#gridBannerKiosko").dxDataGrid('instance');

                            var lookupTienda = grid.columnOption("Tienda", "lookup");
                            lookupTienda.dataSource = data;
                            lookupTienda.update();

                            var lookupHostname = grid.columnOption("Hostname", "lookup");
                            lookupHostname.dataSource = ['TODOS'];
                            lookupHostname.update();
                            grid.repaint();
                        });
                        this.defaultSetCellValue(rowData, value);
                    },
                },
                {
                    dataField: "Tienda",
                    caption: 'Tienda',
                    validationRules: [{ type: "required" }],
                    allowFiltering: false,
                    editorOptions: {
                        searchEnabled: false,
                        placeholder: 'Seleccione una Tienda',
                    },
                    lookup: {
                        dataSource: ['TODOS'],
                    },
                    setCellValue: function (rowData, value) {
                        var params = { Tienda: value };
                        $.post('/Configuracion/Hostnames', params, function (data) {
                            var grid = $("#gridBannerKiosko").dxDataGrid('instance');
                            var lookup = grid.columnOption("Hostname", "lookup");
                            lookup.dataSource = data;
                            lookup.update();
                            grid.repaint();
                        });
                        this.defaultSetCellValue(rowData, value);
                    },
                },
                {
                    dataField: "Hostname",
                    caption: 'Hostname',
                    validationRules: [{ type: "required" }],
                    allowFiltering: false,
                    editorOptions: {
                        searchEnabled: false,
                        placeholder: 'Seleccione un Hostname',
                    },
                    lookup: {
                        dataSource: ['TODOS'],
                    },
                },
            ],
        });
}

function getTemplateGridClientes(idBanner) {
    return $("<div id = 'gridBannerCliente'>")
        .dxDataGrid({
            noDataText: 'No existen datos',
            dataSource: {
                load: function (loadOptions) {
                    var def = $.Deferred();

                    var nroDoc = '';
                    if (loadOptions.filter) {
                        var value = loadOptions.filter;
                        switch (value[0]) {
                            case 'NroDocumento':
                                nroDoc = value[2];
                                break;
                        }
                    }
                    var oBannerCliente = {
                        IdBanner: idBanner,
                        NroDocumento: nroDoc
                    };

                    var start = loadOptions.skip;
                    var end = loadOptions.take;
                    var param = {
                        bannerCliente: oBannerCliente,
                        start: start,
                        end: end
                    };

                    $.post('/BannerCliente/List', param, function (data) {
                        def.resolve(data.lstBannerClienteVM, { totalCount: data.count });
                    }, 'json');
                    return def.promise();
                },
                insert: function (values) {
                    values.IdBanner = idBanner;
                    return $.ajax({
                        url: '/BannerCliente/Create',
                        method: 'POST',
                        contentType: 'application/json',
                        data: JSON.stringify(values)
                    })
                },
                remove: function (key) {
                    return $.ajax({
                        url: '/BannerCliente/Delete',
                        method: 'POST',
                        data: key
                    })
                },
                update: function (key, values) {
                    var object = $.extend({}, key, values);
                    return $.ajax({
                        url: '/BannerCliente/Edit',
                        method: 'POST',
                        contentType: 'application/json',
                        data: JSON.stringify(object)
                    })
                },
            },
            showBorders: true,
            rowAlternationEnabled: true,
            editing: {
                allowAdding: true,
                allowUpdating: true,
                allowDeleting: true,
                mode: 'form',
                texts: getTextsGrid()
            },
            remoteOperations: {
                paging: true,
                filtering: true
            },
            rowAlternationEnabled: true,
            loadPanel: {
                enabled: 'auto',
                height: 100,
                showIndicator: true,
                showPane: true,
                text: 'Cargarndo...',
                width: 200
            },
            pager: {
                allowedPageSizes: 'auto',
                infoText: 'Pagina {0} de {1} ({2} registros)',
                showInfo: true,
                showNavigationButtons: true,
                showPageSizeSelector: false,
                visible: 'auto'
            },
            paging: {
                enabled: true,
                pageIndex: 0,
                pageSize: 4
            },
            filterRow: {
                visible: true,
                applyFilter: "auto",
                showOperationChooser: false
            },
            columns: [
                {
                    dataField: "TipoDocumento",
                    caption: 'Documento',
                    validationRules: [{ type: "required" }],
                    editorOptions: {
                        searchEnabled: false
                    },
                    lookup: {
                        dataSource: ['DNI', 'OTRO'],
                    },
                    allowFiltering: false,
                },
                {
                    dataField: "NroDocumento",
                    caption: 'Numero',
                    validationRules: [{ type: "required" }],
                },
            ],
        });
}

function getTextsGrid() {
    return {
        addRow: 'Agregar',
        cancelRowChanges: 'Cancelar',
        deleteRow: 'Eliminar',
        editRow: 'Editar',
        saveRowChanges: 'Guardar',
        confirmDeleteMessage: '¿Desea eliminar el registro?'
    };
}