﻿using Dapper.Contrib.Extensions;
using System;

namespace Proyecto.Models
{
    [Table("Banner.BannerKioskoProgramacion")]
    public class BannerKioskoProgramacion
    {
        [Key]
        public int IdBannerKioskooProgramacion { get; set; }
        public int IdBannerKiosko { get; set; }
        public DateTime Fecha { get; set; }
        public TimeSpan HoraInicio { get; set; }
        public TimeSpan HoraFin { get; set; }
        public DateTime FechaRegistro { get; set; }
        public bool Sincronizado { get; set; } = false;
    }
}
