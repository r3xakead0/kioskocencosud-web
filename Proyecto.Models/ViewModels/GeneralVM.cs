﻿namespace Proyecto.Models.ViewModels
{
    public class General
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }
}
