﻿namespace Proyecto.Models.ViewModels
{
    public class BannerClienteVM
    {
        public int IdBannerCliente { get; set; }
        public int IdBanner { get; set; }
        public string TipoDocumento { get; set; } = "";
        public string NroDocumento { get; set; } = "";
    }
}
