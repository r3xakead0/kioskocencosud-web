﻿using System;

namespace Proyecto.Models.ViewModels
{
    public class BannerProgramacionVM
    {
        public int IdBannerKioskooProgramacion { get; set; }
        public int IdBannerKiosko { get; set; }
        public string Compania { get; set; }
        public string Tienda { get; set; }
        public string Hostname { get; set; }
        public DateTime Fecha { get; set; }
        public TimeSpan HoraInicio { get; set; }
        public TimeSpan HoraFin { get; set; }
    }
}
