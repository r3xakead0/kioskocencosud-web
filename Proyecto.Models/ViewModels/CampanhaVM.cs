﻿using System.ComponentModel.DataAnnotations;

namespace Proyecto.Models.ViewModels
{
    public class CampanhaVM
    {
        public int IdCampanha { get; set; }
        [Required]
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public bool Activo { get; set; }
    }
}
