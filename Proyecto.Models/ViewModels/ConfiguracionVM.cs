﻿using Dapper.Contrib.Extensions;

namespace Proyecto.Models.ViewModels
{
    public class ConfiguracionVM
    {
        [Key]
        public int Id { get; set; }
        public string Compania { get; set; }
        public string Tienda { get; set; }
        public string Hostname { get; set; }
    }
}
