﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto.Models.ViewModels
{
    public class BannerVM
    {
        public int IdBanner { get; set; }
        public int IdCampanha { get; set; }

        [Required]
        public string Nombre { get; set; }
        [Required]
        [Display(Name = "Archivo de Imagen")]
        public string Imagen { get; set; }
        [Required]
        public int Prioridad { get; set; }
        [Required]
        public int Tiempo { get; set; }

        [Required]
        [Display(Name = "Inicio de Vigencia")]
        public DateTime InicioVigencia { get; set; }
        [Required]
        [Display(Name = "Final de Vigencia")]
        public DateTime FinalVigencia { get; set; }

        [Required]
        public string Tipo { get; set; }

        [Required]
        [Display(Name = "Limite de Vistas")]
        public int VistasLimite { get; set; }

        [Display(Name = "Vistas Actuales")]
        public int VistasActual { get; set; }

        [Required]
        [Display(Name = "Codigos Materiales")]
        public string CodigosMateriales { get; set; } = "";

        public bool Activo { get; set; }

    }
}
