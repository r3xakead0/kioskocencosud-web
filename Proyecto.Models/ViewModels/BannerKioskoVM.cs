﻿namespace Proyecto.Models.ViewModels
{
    public class BannerKioskoVM
    {
        public int IdBannerKiosko { get; set; }
        public int IdBanner { get; set; }
        public string Compania { get; set; } = "";
        public string Tienda { get; set; } = "";
        public string Hostname { get; set; } = "";
    }
}
