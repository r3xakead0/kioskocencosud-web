﻿using Dapper.Contrib.Extensions;
using System;

namespace Proyecto.Models
{
    [Table("Maestro.Campanha")]
    public class Campanha
    {
        [Key]
        public int IdCampanha { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public bool Activo { get; set; }
        public DateTime FechaRegistro { get; set; }
        public bool Sincronizado { get; set; } = false;
    }
}
