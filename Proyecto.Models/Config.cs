﻿using Dapper.Contrib.Extensions;

namespace Proyecto.Models
{
    [Table("Perfil.Config")]
    public class Config
    {
        [Key]
        public int Id { get; set; }
        public string Compania { get; set; }
        public string Tienda { get; set; }
        public string Ubicacion { get; set; }
        public double LongPapel { get; set; }
        public double LongCupon { get; set; }
        public double TotImpresiones { get; set; }
        public double RestImpresiones { get; set; }
        public string Telefono { get; set; }
        public string Email { get; set; }
        public string Servidor { get; set; }
        public int Puerto { get; set; }
        public string EmailSalida { get; set; }
        public string Pass { get; set; }
        public string PassAplicacion { get; set; }
        public string Hostname { get; set; }
        public bool Activo { get; set; } = false;
    }
}
