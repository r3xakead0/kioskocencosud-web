﻿using Dapper.Contrib.Extensions;
using System;

namespace Proyecto.Models
{
    [Table("Banner.BannerKiosko")]
    public class BannerKiosko
    {
        [Key]
        public int IdBannerKiosko { get; set; }
        public int IdBanner { get; set; }
        public string Tienda { get; set; }
        public string Hostname { get; set; }
        public DateTime FechaRegistro { get; set; }
        public bool Sincronizado { get; set; } = false;
    }
}
