﻿using Dapper.Contrib.Extensions;
using System;

namespace Proyecto.Models
{
    [Table("Maestro.Banner")]
    public class Banner
    {
        [Key]
        public int IdBanner { get; set; }
        public int IdCampanha { get; set; }
        public string Nombre { get; set; }
        public string Imagen { get; set; }
        public int Prioridad { get; set; }
        public int Tiempo { get; set; }
        public DateTime InicioVigencia { get; set; }
        public DateTime FinalVigencia { get; set; }
        public bool Activo { get; set; }
        public string Tipo { get; set; }
        public int VistasLimite { get; set; }
        public int VistasActual { get; set; }
        public DateTime FechaRegistro { get; set; }
        public string CodigosMateriales { get; set; }
        public bool Sincronizado { get; set; } = false;
    }
}
