﻿using Dapper.Contrib.Extensions;
using System;

namespace Proyecto.Models
{
    [Table("Banner.BannerCliente")]
    public class BannerCliente
    {
        [Key]
        public int IdBannerCliente { get; set; }
        public int IdBanner { get; set; }
        public string TipoDocumento { get; set; }
        public string NroDocumento { get; set; }
        public DateTime FechaRegistro { get; set; }
        public bool Sincronizado { get; set; } = false;
    }
}
